#!/usr/bin/env node

/** Purpose: easy to use meditative course generation algorithm
    Format: array representing words or number quantity.
    number of seconds that entire thing is including saying text.
    Can also set the frequency of the meditation melodies.
    -- merges silence and music/saying, then
    00 can make objects like { "duration":"200", "saying":"something",
    "frequency":"30.0", volume: "0.6", silence: false };
    can then copy such object for whole array or w/e.

    Generates a bash script.
*/
"use strict";
const wavFileInfo = require('wav-file-info');

const default_time = 10.0;
const default_hertz = 10.0;

const ttsCommand2 = "pico2wave ";
const default_gender = "f2";
const ttsCommand = "espeak-ng -s 120 -x"

const gathering = [
  {"say": "The age of peace has come, and the easiest way to grow into " + 
    " the New Paradise Timeline, is to cultivate forgiveness, compassion, " + 
    " and unconditional love. ", time: 13, hertz: 0 },
 {music:"music/space-vox.wav"},
 {"say": "For quality assurance, " +
   "this gathering is led by an emotionless robot voice " +
   "so we with you can focus on the content of what is spoken, " +
   " and modify the script during our deliberative assembly. ", 
 "hertz": 12, time: 15},
  // intro
 {"say": "Introducing the executive: " +
   "The Computer Speaker is leading our session. " +
     "The 360 degree Camera is recording the proceedings to share the merits of our practice with YouTube. "+
     "The alms bowl is standing in for our treasurer. " +
     "We have human assistants. "
    , silence: true, time: 22},
 {"say":"let us consecrate this meeting, by calling upon the four corners.",
  "music": "music/nautilus.wav"},
 {"say":"We call upon the stars and the insights they bring.",
   time:7.5, hertz: 7.83},
 {"say":"We call upon the earth and its healing powers.",
   time:7.5, hertz: 7.83},
 {"say":"We call upon the north and the winds that breathe life.",
   time:7.5, hertz: 10.5},
 {"say":"We call upon the east and the light that brings knowledge.",
   time:7.5, hertz: 10.5},
 {"say":"We call upon the south and the soil that gives sustenance.",
   time:7.5, hertz: 10.5},
 {"say":"We call upon the west and the water that mixes it all together.",
   time:7.5, hertz: 10.5},
 {"say":"May all come togther to help we with you,"+
 "align our thoughts words and actions, to be the best for all creation",
   time:15, hertz: 10.5},
 {"say": "please be seated", hertz:0, time:10},
  // agenda
 {"say": "Reading the agenda: " +
   "We with you begin with a minute of laughing meditation. " +
     "Then we have a guided meditation for 20 minutes, " +
     "then have 5 minutes of walking meditation, " +
     "followed by a 20 minute silent sit. " +
     "We'll couple up for a few minutes of gratitude social. " +
     "Then we will have a time of shared reading, " +
     "followed by a deliberative assembly for sharing and improving. " + 
     "We'll then have a time for event announcements. " +
     "We'll finish with five bows of humility, " + 
     "dedicating the merits of our practice," +
     "and free hugs for those interested.",
 time:44, silence: true},
 {music:"music/space-vox.wav", time: 5},
 {"say":"Lets synchronize to the gamma brain wave by laughing for 1 minute."+
   "hahahahahahahahahahahahahaahahahahakhakhakhakhakhakha"+
   "heheheh eheheheeheheheheheehehehe heheeheheheheheheh ehehe"+
   "hohohoo hohohohohohhohohohohohoho hohohohohohohohoho hohhoho"+
   "huhuhuh uhuhuhuhuhuhuhuhuhuhuhuhu huhuhuhuhuhuhuhuhu huhuhuhu"+
   "hahahah aahahahahahahahahahahahah aahahahahahahahaha hahahahah"+
   "hihihih ihihihihihihihihihihihihi hihihihihihihihihi hihihihihi"+
   "hahahahahahahahahahahahahaahahahahakhakhakhakhakhakha"+
   "heheheh eheheheeheheheheheehehehe heheeheheheheheheh ehehe"+
   "hohohoo hohohohohohhohohohohohoho hohohohohohohohoho hohhoho"+
   "huhuhuh uhuhuhuhuhuhuhuhuhuhuhuhu huhuhuhuhuhuhuhuhu huhuhuhu"+
   "hahahah aahahahahahahahahahahahah aahahahahahahahaha hahahahah"+
   "hihihih ihihihihihihihihihihihihi hihihihihihihihihi hihihihihi"+
   "khakhakha  khakhakha khakhakha khakhakha khakhakha khakhakha" +
   "heheheh eheheheeheheheheheehehehe heheeheheheheheheh ehehe"+
   "hohohoo hohohohohohhohohohohohoho hohohohohohohohoho hohhoho"+
   "huhuhuh uhuhuhuhuhuhuhuhuhuhuhuhu huhuhuhuhuhuhuhuhu huhuhuhu"+
   "hahahah aahahahahahahahahahahahah aahahahahahahahaha hahahahah"+
   "hihihih ihihihihihihihihihihihihi hihihihihihihihihi hihihihihi"+
   "khakhakha  khakhakha khakhakha khakhakha khakhakha khakhakha" +
   "hahahaha hahahahahahah hahahahaha hahahaha aahahaahahahah ahahah",
   hertz:40, time:60, overflow: true},
   
 {"say":"We will now proceed with Forgiveness and Compassion Meditation for twenty minutes.", hertz:0},
 
 {"say":"May we with you forgive ourselves", hertz:35},
 {silence:true, time:30.0},
 {"say":"May we with you be safe, see your safety, enjoy.", hertz:35},
 {silence:true, time:30.0},
 {"say":"May we with you be healthy, see your health, enjoy.", hertz:35},
 {silence:true, time:30.0},
 {"say":"May we with you have community, see your community, enjoy.", hertz:35},
 {silence:true, time:30.0},
 {"say":"May we with you have liberty, see your liberty, enjoy.", hertz:35},
 {silence:true, time:30.0},
 {"say":"May we with you feel unconditional love for ourselves", hertz:35},
 {silence:true, time:30.0},
 {"say":"May we with you forgive our close one", hertz:35},
 {silence:true, time:30.0},
 {"say":"May our close one be safe, see their safety, enjoy.", hertz:35},
 {silence:true, time:30.0},
 {"say":"May our close one be healthy, see their health, enjoy.", hertz:35},
 {silence:true, time:30.0},
 {"say":"May our close one have community, see their community, enjoy.", hertz:35},
 {silence:true, time:30.0},
 {"say":"May our close one have liberty, see their liberty, enjoy.", hertz:35},
 {silence:true, time:30.0},
 {"say":"May we with you feel unconditional love for our close one", hertz:35},
 {silence:true, time:30.0},
 {"say":"May we with you forgive that stranger", hertz:35},
 {silence:true, time:30.0},
 {"say":"May that stranger be safe, see their safety, enjoy.", hertz:35},
 {silence:true, time:30.0},
 {"say":"May that stranger be healthy, see their health, enjoy.", hertz:35},
 {silence:true, time:30.0},
 {"say":"May that stranger have community, see their community, enjoy.", hertz:35},
 {silence:true, time:30.0},
 {"say":"May that stranger have liberty, see their liberty, enjoy", hertz:35},
 {silence:true, time:30.0},
 {"say":"May we with you feel unconditional love for that stranger", hertz:35},
 {silence:true, time:30.0},
 {"say":"May we with you forgive that complicated person", hertz:35},
 {silence:true, time:30.0},
 {"say":"May that complicated person be safe, see their safety, enjoy.", hertz:35},
 {silence:true, time:30.0},
 {"say":"May that complicated person be healthy, see their health, enjoy.", hertz:35},
 {silence:true, time:30.0},
 {"say":"May that complicated person have community, see their community, enjoy.", hertz:35},
 {silence:true, time:30.0},
 {"say":"May that complicated person have liberty, see their liberty, enjoy", hertz:35},
 {silence:true, time:30.0},
 {"say":"May we with you feel unconditional love for that complicated person", hertz:35},
 {silence:true, time:30.0},
 {"say":"May we with you forgive all beings.", hertz:35},
 {silence:true, time:30.0},
 {"say":"May all beings be safe, see our safety, enjoy.", hertz:35},
 {silence:true, time:30.0},
 {"say":"May all beings be healthy, see our health, enjoy.", hertz:35},
 {silence:true, time:30.0},
 {"say":"May all beings have community, see our community, enjoy.", hertz:35},
 {silence:true, time:30.0},
 {"say":"May all beings have liberty, see our liberty, enjoy.", hertz:35},
 {silence:true, time:30.0},
 {"say":"May we with you feel unconditional love for all beings", hertz:35},
 {silence:true, time:30.0},
 {"say":"forgiveness and compassion meditation complete", 
 "music": "music/seashore.wav"},
 {"say":"We will now do walking meditation for 5 minutes." +
   "Please rise!"},
 {"music":"music/shrinebell.wav", say:"begin walking", time:10.0},
 {silence:true, time:230},
 {"music":"music/shrinebell.wav", say:"1 minute of walking meditation remaining",
   time:10},
 {silence:true, time:55},
 {"music":"music/shrinebell.wav", say:"walking meditation complete"},
 {"music":"music/evolver.wav", time: 10},
 {"say": "we will now have 20 minutes of silent meditation. "+
   "you may do any meditation you wish. "+
   "examples are: breath awareness, gratitude contemplation,"+
   "loving kindness, or accessing the holy spirit." + 
   "Imagine the world you wish to co-create.",
 "music": "music/wind.wav", time:24},
{silence:true, time:1200},
{say:"meditation complete. let us ramp up to gamma for laughter.", 
  time:30, begin_hertz:7.83, hertz:35},
{say: "hahahahahahahahahahahahahaahahahahakhakhakhakhakhakha"+
  "heheheh eheheheeheheheheheehehehe heheeheheheheheheh ehehe"+
  "hohohoo hohohohohohhohohohohohoho hohohohohohohohoho hohhoho"+
  "huhuhuh uhuhuhuhuhuhuhuhuhuhuhuhu huhuhuhuhuhuhuhuhu huhuhuhu"+
  "hahahah aahahahahahahahahahahahah aahahahahahahahaha hahahahah"+
  "hihihih ihihihihihihihihihihihihi hihihihihihihihihi hihihihihi"+
  "khakhakha  khakhakha khakhakha khakhakha khakhakha khakhakha",
  hertz: 40, overflow: true},
{silence: true, time: 8},
 {"music": "music/cloudbell.wav", time: 4},
 {say: "We will now begin the gratitude social time. " +
   "Couple up with a person that you are least familiar with! " +
   "You have 1 minute to find the person, sit next to them and " +
   " introduce yourselves. ", silence:true, music: "music/space-vox.wav", 
   time: 17},
   {"music": "music/cloudbell.wav", time: 4},
 {silence: true, time: 52},
   {"music": "music/cloudbell.wav", time: 4},
 {say: "now for two minutes one of you listens quietly while the other talks about what you are grateful for", hertz:35, time:10},
   {"music": "music/cloudbell.wav", time: 4},
   {silence:true, time:116},
  {"music": "music/cloudbell.wav", time: 4},
  {say: "it is now time to switch quiet listening and gratitude speaking roles.", hertz:40, time: 8},
  {"music": "music/cloudbell.wav", time: 4},
  {silence: true, time:112},
  {"music": "music/cloudbell.wav", time: 4},
  {say: "you now have 4 minutes to socialize with your partner.",
    hertz:30, time:5},
  {"music": "music/cloudbell.wav", time: 4},
  {silence:true, time: 230},
  {"music": "music/cloudbell.wav", time: 4},
  {say: "you now have 1 minute to get back to your seats", 
    silence: true},
  {"music": "music/cloudbell.wav", time: 4},
  {silence: true, time: 56},
  // reading time
  {"music": "music/tubebell.wav", time: 4},
  {"say":"We now have 10 minutes of shared reading time. " +
    "we will pass around the text, and everyone that wants to " +
    "can read a paragraph or three.", time: 12,
    silence:true},
  {"music": "music/tubebell.wav", time: 4},
  {"silence": true, time: 608},
  {"music": "music/tubebell.wav", time: 4},
  // sharing time
  // roberts rules motion time
  {"say": "we will now have a period of sharing, questions and answers. " +
      "We are using Roberts Rules of Order Parliamentary Authority. " +
      "Raise your hand if you wish to speak. " +
      "Soft speaking time limit is 2 minutes. "+
      "Please give everyone a chance to speak before you speak a second time. " +
      "To share start with: point of interest, or I'd like to share. " +
      "To ask a question start with: point of inquiry, or I'd like to ask. " +
      "If there is a question you can share how you solved a similar problem. " +
      "To suggest an improvement to our organization start with: I motion or I'd to to suggest. " +
      "We only have one main motion on the floor at a time. "  +
      "Motions can be seconded, objected to, or amended. ",
  time: 60, hertz: 14},
  {"music": "music/ytubebell.wav"},
  {"say": "This meeting is now in order." +
    "We will begin with summary from the Treasurer" +
    "and course information before opening the floor.",
  gender: "m"},
// treasurer
  {"say": "Treasury summary: " + 
  "our current expenditures are 79 dollars and 36 cents per month. " +
  "we have enough funds for 11 more months of operation. "
  , hertz: 14, time: 13},
// course description
  {"say": "Green Religion has a three month long " + 
      " Compassionately Mindful Health Course available, " +
      "To pass it you will demonstrate a healthy diet of food " +
      " and physical exercise, " +
      "a regular mind administration  practice. " +
      "and healthy budget habits. " + 
      "After passing you will qualify to join our intentional community. "+
      "The course costs 1 thousand 500, but you may pay or supplement with volunteer hours by " +
      "attending Green Religion events such as this one, or providing related services. " +
      "You get twenty dollars in course credits per hour of volunteering. " +
      " Electronic donations can also go towards your course fee. " +
      " Can download My Course Payment spreadsheet from greenreligion.org "+
      " to fill in your volunteer and fiscal contribution claims. ", 
  time: 60, hertz: 15.66},
  {say: "today on the deliberation agenda we have:" +
    "Today on the deliberation agenda we have approval of the previous " +
    " proceedings video. " +
    "Discussion about advertising strategies such as robocalls or posters. ",
  time: 17},
  {say:  
     " you can share, ask, or suggest improvements. " +
    "The floor is now open for deliberation, ",
    hertz:35, gender: "m", time: 8},
  {"music": "music/ytubebell.wav"},
  {"silence": true, time: 1980},
  {"music": "music/ytubebell.wav"},
  // 2 minutes remaining
  {"say": "there are two minutes of sharing remaining", 
    "music": "music/ytubebell.wav"},
  {silence: true, time: 110},
  {"music": "music/ytubebell.wav"},
// announcements
  {"say": "we now have 2 minutes for people to make a announcements, " +
      "raise your hand if you would like to.",
  "silence": true},
  {silence:true, time: 120},
// three bows to the intuitive mind, the teachings and the community.
  {"say": "we will now have the five bows or shows of humility.",
    music: "music/shrinebell.wav"},
  {"say": "we bow to infinite intelligence and the inspiration it brings.",
    music: "music/shrinebell.wav"},
  {"say": "we bow to the teachings of the world and the knowledge it brings.",
    music: "music/shrinebell.wav"},
  {"say": "we bow to the community and it's support and opportunities for co-creation.",
    music: "music/shrinebell.wav"},
  {"say": "we bow to the ecology, acknowledging our bodies need it for sustenance.",
    music: "music/shrinebell.wav"},
  {"say": "we bow to all creation, remembering that we are but a spark.",
    music: "music/shrinebell.wav"},
// dedicating the merit
  {"say": "we dedicate the merit of our practice to helping all beings live with joy and unconditional love", time: 15},
  {music: "music/shrinebell.wav", time: 5},
  // free hugs
  {say: "Those interested in free hugs can now stand, spread their arms," +
      " and remembering to be gentle walk forward forming a group hug " +
      " in the centre. One on one hugs are also acceptable.", 
      hertz: 40,
      time: 15},
  {music: "music/shrinebell.wav"},
  {silence: true, time: 20},
  {say: "gathering complete. Go forth as beacons of peace.", music:"music/space-vox.wav"}
];

function establish_final_sox_command(count) {
  let produce = "sox ";
  for(let i = 0; i < count; i++) {
    produce += `${i}.wav `;
  }
  produce += "out.wav;\n";
  produce += "rm ";
  for(let i = 0; i < count; i++) {
    produce += `${i}.wav `;
  }
  produce += ";\n"
  return produce;
}

const default_volume = 20;
function main() { 
  let produce = "";
  produce += "#!/bin/bash\n"
  let full_command = "";
  let time = default_time;
  let volume = 20;
  let bps = 1;
  let begin_bps = bps;
  let hertz = 0;
  let begin_hertz = 0;
  let say = ""
  let gender = default_gender;
  for (var i = 0; i < gathering.length; i++) {

    volume = gathering[i].silence == true || gathering[i].hertz == 0? 0: 
      default_volume;
    say = gathering[i].say == undefined? "": gathering[i].say ;
    gender = gathering[i].gender == undefined? default_gender: 
      gathering[i].gender;
    full_command = "";
    full_command += `${ttsCommand} -v en+${gender} -w ${i}.wav "${say}";\n`;
    full_command += `echo sox ${i}.wav -r 48000 0${i}.wav;\n`;
    full_command += `sox ${i}.wav -r 48000 0${i}.wav;\n`;
    full_command += `rm ${i}.wav;\n`;
    time = gathering[i].time? gathering[i].time: default_time;
    hertz = gathering[i].hertz? gathering[i].hertz: default_hertz;
    begin_hertz = gathering[i].begin_hertz? gathering[i].begin_hertz: hertz;
    bps = gathering[i].hertz <= 12?  bps = 1: bps = gathering[i].hertz;
    begin_bps = gathering[i].begin_hertz? begin_hertz: bps;
    if (gathering[i].music) {
      full_command += `sox ${gathering[i].music} --channels 1 -r 48000 m${i}.wav;\n`;
    } else {
    full_command += `chuck -s aRec.ck:${time} 31tHertzChords.ck:${time}:${begin_bps}:${bps}:${volume}:${volume}:${begin_hertz}:${hertz}:"up";\n`
    full_command += `mv foo.wav m${i}.wav;\n`;
    }
    full_command += `echo sox -m  m${i}.wav 0${i}.wav ${i}.wav  trim 0 ${time};\n`;
    if (gathering[i].overflow != true) {
    full_command += `length=$(sox 0${i}.wav -n stat 2>&1|`
      + `grep Length| awk '{print $3}');\n`;
    full_command += `if (( $(echo "$length > ${time}" |bc -l) )); ` +
       `then echo "ERROR: speech $length cut off for ${say}"; exit 1; fi; \n`;
    }
    full_command += `sox -m  m${i}.wav 0${i}.wav ${i}.wav  trim 0 ${time};\n`;
    full_command += `rm m${i}.wav; rm 0${i}.wav;\n`;
    //full_command += `sox 0${i}.wav -n stat 2>&1 `;
    //  'sed -n \'s#^Length (seconds):[^0-9]*\([0-9.]*\)$#\1#p\'';
    produce += full_command;
  }
  produce += establish_final_sox_command(gathering.length);
  console.log(produce);
  return 0;
}
main();
