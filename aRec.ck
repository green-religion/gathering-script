// chuck this with other shreds to record to file
// example> chuck foo.ck bar.ck rec (see also rec2.ck)

// arguments: rec:<filename>

Std.atoi(me.arg(0)) => int totalSeconds;
// get name
me.arg(1) => string filename;
//Std.strtoi(me.arg(0)) => string filename;
if( filename.length() == 0 ) "foo.wav" => filename;

// pull samples from the dac
dac => Gain g => WvOut w => blackhole;
// this is the output file name
filename => w.wavFilename;
<<<"writing to file:", "'" + w.filename() + "'">>>;
// any gain you want for the output
.8 => g.gain;

// infinite time loop...
// ctrl-c will stop it, or modify to desired duration
totalSeconds::second => now;
