/* arguments are as follows:
hertzMelody.ck:30:40:23:0.8:0.62 */
Std.atof(me.arg(0)) /*30.0*/ => float maxSeconds;
Std.atof(me.arg(1)) /*12.0*/ => float startHertz;
Std.atof(me.arg(2)) /*14.0*/ => float endHertz;
Std.atof(me.arg(3)) /*0.8*/  => float startGain;
Std.atof(me.arg(4)) /*0.62*/ => float endGain;
Std.atof(me.arg(5)) /*30*/ => float startBeatFreq;
Std.atof(me.arg(6)) /*40*/ => float endBeatFreq;
me.arg(7) /*40*/ => string motion;

4 => int minOctave;
5 => int maxOctave;
//4 => int minOctave;
//6 => int maxOctave;
//7 => int maxOctave;
0 => int minNote;
30 => int maxNote;

0  => int unison;
6  => int septimalWholeTone;
7  => int septimalMinorThird;
8  => int minorThird;
9  => int neutralThird;
10 => int majorThird;
11 => int septimalMajorThird;
12 => int septimalNarrowFourth;
13 => int perfectFourth;
18 => int perfectFifth;
25 => int harmonicSeventh;
31 => int octave;

[unison, perfectFourth] @=> int downIntervals[];
[unison, perfectFifth] @=> int upIntervals[];
//[unison] @=> int upIntervals[];
[unison, majorThird, perfectFifth] @=> int majorChord[];
[unison, minorThird, perfectFifth] @=> int minorChord[];
[unison, neutralThird, perfectFifth] @=> int neutralChord[];

[unison, majorThird, perfectFifth, harmonicSeventh] @=> 
  int majorSeventhChord[];
[unison, minorThird, perfectFifth, harmonicSeventh] @=> 
  int minorSeventhChord[];
[unison, neutralThird, perfectFifth, harmonicSeventh] @=> 
  int neutralSeventhChord[];

[unison, septimalMajorThird, perfectFifth, harmonicSeventh]
  @=> int septMajorSeventhChord[];
[unison, septimalMinorThird, perfectFifth, harmonicSeventh]
  @=> int septMinorSeventhChord[];
[unison, septimalWholeTone, septimalNarrowFourth, perfectFifth]
  @=> int septNeutralTritoneChord[];

///SinOsc s1 => dac;


    SinOsc s1 => dac;
    SinOsc s2 => dac;

0.4 => s1.gain;
0.4 => s2.gain;

// 30 second slices
startGain => float currentGain;
startBeatFreq => float currentBeatFreq;
(startHertz+endHertz)/2 => float brainwaveHertz;
1000.0/startHertz => float noteDuration;
1000.0/endHertz => float goalDuration;
brainwaveHertz*maxSeconds => float maxIterations;
0 => float totalDuration;
int note;
int rootNote;
float noteFreq;
float noteIncrement;
float gainIncrement;
float beatIncrement;
float iterationsLeft;
int previousNote;
int previousOctave;
int rootOctave;
int interval;
Math.rand2(minNote, maxNote) => rootNote;
Math.rand2(minNote, maxNote) => note;
Math.rand2(minOctave, maxOctave) => rootOctave;
Math.fabs((goalDuration-noteDuration))/maxIterations 
    => noteIncrement;

fun float getKeyFreq(int rootOctave, int note, int tet, float base) {
    rootOctave * tet + note => int noteNumber;
    4 * tet + 1 => int rootNote;
    (noteNumber - rootNote)$ float / (tet $float) => float exponent;
    return Math.pow(2, exponent) * base;
}
fun float petaFreq(int rootOctave, int note) {
    // petatonic as all sharps of 12tet
    5 => int pnote; // C# default
    if (note == 1) {
        2 => pnote; // A#
    }
    if (note == 2) {
        5 => pnote; // C#
    }
    if (note == 3) {
        7 => pnote; // D#
    }
    if (note == 4) {
        10 => pnote; // F#
    }
    if (note == 5) {
        12 => pnote; // G#
    }
    return getKeyFreq(rootOctave, pnote, 12, 440);
}

fun int[] adjustLowNote(int theNote, int theOctave) {
      if (theNote < minNote && theOctave != minOctave) {
          theOctave - 1 => theOctave;
          maxNote - Math.abs(theNote) % maxNote => theNote;
      }
      if (theNote < minNote && theOctave == minOctave) {
          maxOctave => theOctave;
          maxNote - Math.abs(theNote) % maxNote => theNote;
      }
      return [theOctave, theNote];
}

fun int[] adjustHighNote(int theNote, int theOctave) {
      if (theNote > maxNote && theOctave != maxOctave) {
          theOctave+1 => theOctave;
          theNote % maxNote => theNote;
      }
      if (theNote > maxNote && theOctave == maxOctave) {
          minOctave => theOctave;
          theNote % maxNote => theNote;
      }
      return [theOctave, theNote];
}

fun int[] nextRootNote(string motion, int theNote, int theOctave) {
  //<<<"nextRootNote Start " + theNote + " Octave " + theOctave + " motion " +
  // motion>>>;
  if (motion == "level") {
    Math.rand2(minOctave, maxOctave) => theOctave;
    Math.rand2(minNote, maxNote) => theNote;
  }
  if (motion == "up") {
    // circle of perfect fifths
    upIntervals[Math.rand2(0, upIntervals.cap() - 1)] => interval; 
    //<<< "NRN i " + interval >>>;
    Math.rand2(0,1) => int direction;
    if (direction == 0) {
      (theNote - (octave - interval)) => theNote;
      //<<< "NRN u TN " + theNote>>>;
      adjustLowNote(theNote, theOctave) @=> int rootOctaveNote[];
      rootOctaveNote[0] => theOctave;
      rootOctaveNote[1] => theNote;
      
    } else {
      (theNote + interval) => theNote;
      //<<< "NRN d TN " + theNote>>>;
      adjustHighNote(theNote, theOctave) @=> int rootOctaveNote[];
      rootOctaveNote[0] => theOctave;
      rootOctaveNote[1] => theNote;
    }
  }
  if (motion == "down") {
    // circle of perfect fourths
    downIntervals[Math.rand2(0,downIntervals.cap()-1)] 
        => interval; 
    Math.rand2(0,1) => int direction;
    if (direction == 0) {
      (theNote - (octave - interval)) => theNote;
      adjustLowNote(theNote, theOctave) @=> int rootOctaveNote[];
      rootOctaveNote[0] => theOctave;
      rootOctaveNote[1] => theNote;
    } else {
      (theNote + interval) => theNote;
      adjustHighNote(theNote, theOctave) @=> int rootOctaveNote[];
      rootOctaveNote[0] => theOctave;
      rootOctaveNote[1] => theNote;
    }
  }
  //<<< "root theNote " + theNote + " theOctave " + theOctave >>>;
  return [theOctave, theNote];
}

7 => int arpeggioLength;
0 => int arpeggioSpot;
4 => int chordLength;

for (0.0 => float i; i < maxIterations; i+1.0 => i) {
(maxSeconds*1000-totalDuration)/noteDuration => iterationsLeft;
iterationsLeft + i => maxIterations;
(goalDuration-noteDuration)/(iterationsLeft$int+1) => noteIncrement;
(endGain-currentGain)/(iterationsLeft$int+1) => gainIncrement;
(endBeatFreq-currentBeatFreq)/(iterationsLeft$int+1) => beatIncrement;
    //<<< "maxIterations " + maxIterations >>>;
    //<<< "i " + i >>>;
    if (Math.fmod(i,2)== 1.0 && brainwaveHertz > 4.0) { // isochronic 
      0 => s1.freq;
      0 => s2.freq;
    }
    else {
      //<<<"arpeggioSpot "+ arpeggioSpot>>>;
      if (arpeggioSpot == arpeggioLength) {
        0 => arpeggioSpot; // simple chords
        nextRootNote(motion, rootNote, rootOctave) @=> int rootOctaveNote[];
        rootOctaveNote[0] => rootOctave;
        rootOctaveNote[1] => rootNote;
        rootNote => note;
      }
      Math.rand2(0, 1) => int septimal;
      int chordSpot;
      if (arpeggioSpot < chordLength) {
        arpeggioSpot => chordSpot;
      } else {
        chordLength - 1 - (arpeggioSpot % chordLength) => chordSpot;
      } 
      if (motion == "up") {
        if (septimal == 0) {
          rootNote + majorSeventhChord[chordSpot] => note;
        } else {
          rootNote + septMajorSeventhChord[chordSpot] => note;
        }
      }
      if (motion == "down") {
        if (septimal == 0) {
          rootNote + minorSeventhChord[chordSpot] => note;
        } else {
          rootNote + septMinorSeventhChord[chordSpot] => note;
        }
      }
      if (motion == "level") {
        if (septimal == 0) {
          rootNote + neutralSeventhChord[chordSpot] => note;
        } else {
          rootNote + septNeutralTritoneChord[chordSpot] => note;
        }
      }
      adjustHighNote(note, rootOctave) @=> int rootOctaveNote[];
      rootOctaveNote[0] => rootOctave;
      rootOctaveNote[1] => note;
      arpeggioSpot++;
      getKeyFreq(rootOctave, note, 31, 443.0) =>  noteFreq;
     // <<<"rootNote "+ rootNote>>>;
     // <<<"note "+note>>>;
     // <<<"rootOctave "+rootOctave>>>;
     // <<<"freq "+(noteFreq $ int)>>>;
      noteFreq => s1.freq;
      noteFreq + currentBeatFreq => s2.freq;
      currentGain/100 => s1.gain;
      currentGain/100 => s2.gain;
    } 
    noteDuration :: ms => now; 
    noteDuration + noteIncrement => noteDuration;
    currentGain + gainIncrement => currentGain;
    currentBeatFreq + beatIncrement => currentBeatFreq;
    noteDuration +=> totalDuration;
    //<<< "noteFreq "+ noteFreq >>>;
    //<<< "noteIncrement "+ noteIncrement >>>;
    //<<< "gainIncrement "+ gainIncrement>>>;
    //<<< "currentGain/100 "+ currentGain/100>>>;
    //<<< "noteDuration "+ noteDuration>>>;
    //<<< "totalDuration "+ totalDuration/1000>>>;
}
fun float getTetFreq() {
  Math.rand2(1,31) => int note;
  Math.rand2(6, 7) => int rootOctave;
  getKeyFreq(rootOctave, note, 31, 432.0) => float noteFreq;
  return noteFreq;
}

0 => s1.freq;
0 :: ms => now;
<<< "done " >>>;
